FROM python:3.9-slim

WORKDIR /opt/install
ADD ./ /opt/install
RUN python -m pip install -r /opt/install/requirements.txt
RUN python /opt/install/setup.py install

VOLUME /opt/config
VOLUME /opt/database
