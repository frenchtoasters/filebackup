import json
import boto3
import unittest
from backup.backup import SyncShit
from watchdog.events import FileModifiedEvent
from moto import mock_s3
from string import Template


class BackupTest(unittest.TestCase):
    @staticmethod
    def _loadconfig(config_path: str = None):
        with open(config_path, 'r') as file:
            config = json.loads(file.read())
        return SyncShit(**config)

    def test_syncshit_trigger_config(self):
        # TODO::Update the test_syncshit_trigger_config to be better than this
        # NOTES::Does this get past spam filter
        # TAGS::app=syncshit
        sync = self._loadconfig(config_path='../example_config.json')
        self.assertEqual(type(SyncShit().config), type(sync.config))

    def test_syncshit_watch_config(self):
        # TODO::Update the test_syncshit_watch_config to be better than this
        # NOTES::Does this get past spam filter
        # TAGS::app=syncshit
        sync = self._loadconfig(config_path='../example_watch_config.json')
        self.assertEqual(type(SyncShit().config), type(sync.config))

    @mock_s3
    def test_s3_command_trick_trigger(self):
        sync = self._loadconfig(config_path='../example_config.json')
        conn = boto3.client('s3', region_name='us-east-1')
        conn.create_bucket(Bucket=sync.config.get('s3bucket'))

        sync.s3_trigger()

        if not sync.config.get("s3bucket"):
            sync.config["s3bucket"] = None

        event = FileModifiedEvent(src_path=sync.config.get("backup_file_path"))

        if event.is_directory:
            object_type = 'directory'
        else:
            object_type = 'file'

        context = {
            'watch_src_path': event.src_path,
            'watch_dest_path': '',
            'watch_event_type': event.event_type,
            'watch_object': object_type,
        }

        if sync.config.get('s3bucket') is None:
            if hasattr(event, 'dest_path'):
                context.update({'dest_path': event.dest_path})
                bucket = 'echo "${watch_event_type} ${watch_object} from ${watch_src_path} to ${watch_dest_path}"'
            else:
                bucket = 'echo "${watch_event_type} ${watch_object} ${watch_src_path}"'

        else:
            if hasattr(event, 'dest_path'):
                context.update({'watch_dest_path': event.dest_path})
            bucket = sync.config.get('s3bucket')

        bucket = Template(bucket).safe_substitute(**context)
        # Creates files in directory for key
        response = conn.upload_file(event.src_path, bucket, f"{sync.config.get('s3key')}/{str(event.src_path).strip('./').split('/')[-1]}")
        self.assertEqual(None, response)


unittest.main()
