from setuptools import setup, find_packages
import backup

setup(
    name='filebackup',
    version=backup.__version__,
    author=backup.__author__,
    description='s3 backup for keepass database files, or other files',
    long_description=open('README.md').read(),
    url='https://gitlab.com/frenchtoasters/keepassbackup',
    keywords=['cli', 'backup'],
    license="Dont say i didnt give you something",
    packages=find_packages(exclude=['*.test', '*.test.*']),
    include_package_data=True,
    install_requires=open('requirements.txt').readlines(),
    entry_points={
        'console_scripts': [
            'filebackup=backup.backup:main'
        ]
    }
)
