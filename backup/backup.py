import logging
import json
import sys
import time
from logging.handlers import RotatingFileHandler
from botocore.exceptions import ClientError
from watchdog.tricks import Trick
from watchdog.utils import WatchdogShutdown
from watchdog.events import FileModifiedEvent
from watchdog.events import FileSystemEvent


class S3CommandTrick(Trick):
    """Executes upload to configured s3 bucket using boto3 in response to matched events."""
    # TODO::Add more options for s3 command here to provide all supported flags for uploading to s3
    # NOTES::Does this get past spam filter
    # TAGS::app=syncshit
    def __init__(self, s3_bucket: str = None, s3_key: str = None, patterns=None, ignore_patterns=None,
                 ignore_directories: bool = False, wait_for_process: bool = False,
                 drop_during_process: bool = False, access_key: str = None, secret_key: str = None,
                 backup_file_path: str = None):
        super().__init__(
            patterns=patterns, ignore_patterns=ignore_patterns,
            ignore_directories=ignore_directories)
        self.s3_bucket = s3_bucket
        self.s3_key = s3_key
        self.s3_access_key = access_key
        self.s3_secret_key = secret_key
        self.wait_for_process = wait_for_process
        self.drop_during_process = drop_during_process
        self.process = None
        self.backup_file_path = backup_file_path

    def trigger_event(self, event: FileSystemEvent = None):
        from string import Template
        import boto3
        if self.drop_during_process and self.process and self.process.poll() is None:
            return

        if event.is_directory:
            object_type = 'directory'
        else:
            object_type = 'file'

        context = {
            'watch_src_path': event.src_path,
            'watch_dest_path': '',
            'watch_event_type': event.event_type,
            'watch_object': object_type,
        }

        if self.s3_bucket is None:
            if hasattr(event, 'dest_path'):
                context.update({'dest_path': event.dest_path})
                bucket = 'echo "${watch_event_type} ${watch_object} from ${watch_src_path} to ${watch_dest_path}"'
            else:
                bucket = 'echo "${watch_event_type} ${watch_object} ${watch_src_path}"'

        else:
            if hasattr(event, 'dest_path'):
                context.update({'watch_dest_path': event.dest_path})
            bucket = self.s3_bucket

        bucket = Template(bucket).safe_substitute(**context)
        if self.s3_secret_key:
            s3 = boto3.client('s3', aws_access_key_id=self.s3_access_key, aws_secret_access_key=self.s3_secret_key)
        else:
            s3 = boto3.client('s3')
        if context.get('watch_event_type') == "modified":
            try:
                # Creates files in directory for key
                logging.info(f"Uploading to: {self.s3_key}/{str(event.src_path).split('/')[-1]}")
                s3.upload_file(event.src_path, bucket, f"{self.s3_key}/{str(event.src_path).split('/')[-1]}")
            except ClientError:
                raise

    def on_any_event(self, event: FileSystemEvent = None):
        from string import Template
        import boto3
        from botocore.exceptions import ClientError

        if self.drop_during_process and self.process and self.process.poll() is None:
            return

        if event.is_directory:
            object_type = 'directory'
        else:
            object_type = 'file'

        context = {
            'watch_src_path': event.src_path,
            'watch_dest_path': '',
            'watch_event_type': event.event_type,
            'watch_object': object_type,
        }

        if self.s3_bucket is None:
            if hasattr(event, 'dest_path'):
                context.update({'dest_path': event.dest_path})
                bucket = 'echo "${watch_event_type} ${watch_object} from ${watch_src_path} to ${watch_dest_path}"'
            else:
                bucket = 'echo "${watch_event_type} ${watch_object} ${watch_src_path}"'

        else:
            if hasattr(event, 'dest_path'):
                context.update({'watch_dest_path': event.dest_path})
            bucket = self.s3_bucket

        bucket = Template(bucket).safe_substitute(**context)
        s3 = boto3.client('s3')
        if event.src_path.split('/')[-1] in self.backup_file_path.split('/')[-1]:
            try:
                # Creates files in directory for key
                logging.info(f"Uploading to: {self.s3_key}/{str(event.src_path).strip('./').split('/')[-1]}")
                response = s3.upload_file(event.src_path, bucket, f"{self.s3_key}/{str(event.src_path).strip('./').split('/')[-1]}")
            except ClientError as e:
                logging.error(e)
                return False
            return response


class SyncShit(object):
    def __init__(self, directories: str = ".", patterns: str = "*", ignore_patterns: str = "",
                 ignore_directories: bool = False, recursive: bool = False, timeout: float = 10.0,
                 wait_for_process: bool = True, drop_during_process: bool = False, s3bucket: str = None,
                 s3key: str = None, backup_file_path: str = "keeyfile.kdbx", aws_access_key: str = None,
                 aws_secret_key: str = None, log_file_path: str = "/var/log/keepassbackup.log", action: str = None):
        logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
        rotate_handler = RotatingFileHandler(filename=log_file_path, mode='a', maxBytes=10000000, backupCount=2,
                                             delay=False)
        self.logger = logging.getLogger()
        self.logger.addHandler(rotate_handler)
        self.config = {
            "patterns": patterns,
            "ignore_patterns": ignore_patterns,
            "ignore_directories": ignore_directories,
            "wait_for_process": wait_for_process,
            "drop_during_process": drop_during_process,
            "timeout": timeout,
            "directories": directories,
            "recursive": recursive,
            "s3bucket": s3bucket,
            "s3key": s3key,
            "aws_access_key": aws_access_key,
            "aws_secret_key": aws_secret_key,
            "backup_file_path": backup_file_path,
            "action": action
        }
        self.patterns = patterns
        self.ignore_patterns = ignore_patterns
        self.ignore_directories = ignore_directories
        self.wait_for_process = wait_for_process

    @staticmethod
    def parse_patterns(patterns_spec, ignore_patterns_spec, separator=';'):
        """
        Parses pattern argument specs and returns a two-tuple of
        (patterns, ignore_patterns).
        """
        patterns = patterns_spec.split(separator)
        ignore_patterns = ignore_patterns_spec.split(separator)
        if ignore_patterns == ['']:
            ignore_patterns = []
        return patterns, ignore_patterns

    def observe_with(self, observer, event_handler, pathnames, recursive):
        """
        Single observer thread with a scheduled path and event handler.
        :param observer:
            The observer thread.
        :param event_handler:
            Event handler which will be called in response to file system events.
        :param pathnames:
            A list of pathnames to monitor.
        :param recursive:
            ``True`` if recursive; ``False`` otherwise.
        """
        for pathname in set(pathnames):
            observer.schedule(event_handler, pathname, recursive)
        observer.start()
        try:
            while True:
                time.sleep(1)
        except WatchdogShutdown:
            observer.stop()
        observer.join()

    def s3_trigger(self):
        """
        Subcommand to execute shell commands in response to file system events.
        :param self.config:
            Configuration argument options.
        """
        if not self.config.get("s3bucket"):
            self.config["s3bucket"] = None

        patterns, ignore_patterns = self.parse_patterns(self.config.get("patterns"),
                                                        self.config.get("ignore_patterns"))
        handler = S3CommandTrick(s3_bucket=self.config.get("s3bucket"),
                                 s3_key=self.config.get("s3key"),
                                 patterns=patterns,
                                 ignore_patterns=ignore_patterns,
                                 ignore_directories=self.config.get("ignore_directories"),
                                 wait_for_process=self.config.get("wait_for_process"),
                                 drop_during_process=self.config.get("drop_during_process"),
                                 access_key=self.config.get("aws_access_key"),
                                 secret_key=self.config.get("aws_secret_key"),
                                 backup_file_path=self.config.get("backup_file_path"))
        event = FileModifiedEvent(src_path=self.config.get("backup_file_path"))
        try:
            handler.trigger_event(event=event)
        except ClientError:
            raise
        self.logger.info(f"Upload complete")

    def s3_watch(self):
        """
        Subcommand to execute shell commands in response to file system events.
        :param self.config:
            Configuration argument options.
        """
        if not self.config.get("s3bucket"):
            self.config["s3bucket"] = None

        from watchdog.observers.polling import PollingObserver as Observer

        patterns, ignore_patterns = self.parse_patterns(self.config.get("patterns"),
                                                        self.config.get("ignore_patterns"))
        self.logger.info(f"Registering s3 upload to bucket: {self.config.get('s3bucket')}")
        handler = S3CommandTrick(s3_bucket=self.config.get("s3bucket"),
                                 s3_key=self.config.get("s3key"),
                                 patterns=patterns,
                                 ignore_patterns=ignore_patterns,
                                 ignore_directories=self.config.get("ignore_directories"),
                                 wait_for_process=self.config.get("wait_for_process"),
                                 drop_during_process=self.config.get("drop_during_process"),
                                 backup_file_path=self.config.get("backup_file_path"))
        observer = Observer(timeout=self.config.get("timeout"))
        self.logger.info(f"Looking at directory: {self.config.get('directories')}")
        self.observe_with(observer, handler, self.config.get('directories'), self.config.get("recursive"))
        self.logger.info(f"Register of s3 COMPLETE, for bucket: {self.config.get('s3bucket')}")


def main():
    # TODO::Validate config path is valid path
    # NOTES::Need to validate that its a valid path and correctly close file
    # TAGS::app=syncshit,type=bug
    config_path = sys.argv[1]
    config = json.loads(open(config_path, 'r').read())
    sync = SyncShit(**config)
    if sync.config.get('action') == "watch" or sync.config.get('action') is not None:
        logging.info(f"Watching directory: {sync.config.get('directories')}")
        try:
            sync.s3_watch()
        except ClientError as e:
            logging.info(e)
    else:
        try:
            sync.s3_trigger()
        except ClientError as e:
            logging.info(e)
    logging.info(f"Upload Complete: {sync.config.get('s3key')}")


if __name__ == "__main__":
    main()

